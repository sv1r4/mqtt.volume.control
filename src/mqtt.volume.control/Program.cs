﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mqtt.volume.control.alsa;
using mqtt.volume.control.config;
using mqtt.volume.control.extensions;
using mqtt.volume.control.mqtt;
using mqtt.volume.control.mqtt.config;
using mqtt.volume.control.mqtt.model;

namespace mqtt.volume.control
{
    class Program
    {
        private static IServiceProvider _serviceProvider;
        private static ILogger _logger;
        private static IMqttAdapter _mqtt;
        private static IVolumeControl _volume;
        private static AppConfig _config;
        static async Task Main()        
        {

            using (var cts = new CancellationTokenSource())
            {
                var token = cts.Token;
                Console.CancelKeyPress += (sender, args) =>
                {
                    Console.WriteLine("App graceful stop");
                    args.Cancel = true;
                    try
                    {
                        _mqtt?.StopAsync().GetAwaiter().GetResult();
                    }
                    finally
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        cts.Cancel();
                    }
                };
                Console.WriteLine("App start");

                Setup();
                
                _logger = _serviceProvider.GetService<ILogger<Program>>();
                _logger.LogInformation("Run Mqtt Volume Control");

                _config = _serviceProvider.GetService<IOptions<AppConfig>>().Value;
                
                _volume = _serviceProvider.GetRequiredService<IVolumeControl>();
                _mqtt = _serviceProvider.GetRequiredService<IMqttAdapter>();
                await InitMqtt();
                
                token.WaitHandle.WaitOne();
            }
        }

        private static async Task InitMqtt()
        {
            _mqtt.OnMqttMessage += OnMqttMessage;
            await _mqtt.ConnectAsync();
            await SubscribeTopicsAsync();
        }

        private static async Task SubscribeTopicsAsync()
        {
            await _mqtt.SubscribeAsync(_config.BaseTopic.CommandWildcard());
        }

        private static async void OnMqttMessage(object sender, Message e)
        {
            try
            {
                if (string.Equals(e.Topic, _config.BaseTopic.CommandSetVolume(), StringComparison.OrdinalIgnoreCase))
                {
                    var volPercent = int.Parse(e.Payload);
                    await _volume.SetVolumeAsync(volPercent);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Handle mqtt message error");
            }
        }

        private static void Setup()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            var services = new ServiceCollection();
            services.AddOptions();
            services.AddLogging(l => l.AddConsole()
                    .AddConfiguration(config.GetSection("Logging")));

            services.Configure<MqttConfig>(config.GetSection(nameof(MqttConfig)));
            services.Configure<AppConfig>(config.GetSection(nameof(AppConfig)));
            services.Configure<AlsaConfig>(config.GetSection(nameof(AlsaConfig)));
            services.AddTransient<IMqttAdapter, MqttNetAdapter>();
            services.AddTransient<IVolumeControl, AlsaCliWrapper>();

            _serviceProvider = services.BuildServiceProvider();

        }
    }
}
