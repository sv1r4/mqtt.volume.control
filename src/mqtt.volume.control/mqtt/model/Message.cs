﻿namespace mqtt.volume.control.mqtt.model
{
    public class Message
    {
        public string Topic { get; set; }
        public string Payload { get; set; }
    }
}
