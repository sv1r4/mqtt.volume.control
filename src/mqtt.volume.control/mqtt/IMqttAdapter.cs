﻿using System;
using System.Threading.Tasks;
using mqtt.volume.control.mqtt.model;

namespace mqtt.volume.control.mqtt
{
    public interface IMqttAdapter
    {
        Task StopAsync();
        Task ConnectAsync();
        Task SubscribeAsync(string topic);

        event EventHandler<Message> OnMqttMessage;
    }
}
