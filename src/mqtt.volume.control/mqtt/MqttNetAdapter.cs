﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mqtt.volume.control.config;
using mqtt.volume.control.mqtt.config;
using mqtt.volume.control.mqtt.model;
using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

namespace mqtt.volume.control.mqtt
{
    public class MqttNetAdapter:IMqttAdapter
    {
        private readonly IManagedMqttClient _mqtt;
        private readonly IManagedMqttClientOptions _mqttOptions;
        // ReSharper disable PrivateFieldCanBeConvertedToLocalVariable
        private readonly ILogger _logger;
        private readonly MqttConfig _config;
        // ReSharper restore PrivateFieldCanBeConvertedToLocalVariable
        
        public event EventHandler<Message> OnMqttMessage;

        public MqttNetAdapter(ILogger<MqttNetAdapter> logger, IOptions<MqttConfig> config)
        {
            _logger = logger;
            _config = config.Value;
            var optionBuilder = new MqttClientOptionsBuilder()
                .WithClientId($"mqtt-alsa-control-{Guid.NewGuid()}")
                .WithTcpServer(_config.Host, _config.Port);

            if (!string.IsNullOrWhiteSpace(_config.User))
            {
                optionBuilder = optionBuilder.WithCredentials(_config.User, _config.Password);
            }

            _mqttOptions = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(optionBuilder.Build())
                .Build();

            _mqtt = new MqttFactory().CreateManagedMqttClient();
            _mqtt.UseApplicationMessageReceivedHandler(e =>
            {
                var topic = e.ApplicationMessage.Topic;
                var message = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                _logger.LogDebug($"Got mqtt message topic='{topic}' message='{message}'");
                OnMqttMessage?.Invoke(this, new Message
                {
                    Topic = topic,
                    Payload = message
                });
            });
            _mqtt.UseConnectedHandler(e =>
            {
                _logger.LogDebug(
                    $"Mqtt connect '{_config.Host}:{_config.Port}' result {e.AuthenticateResult.ResultCode.ToString()}");
            });

            _mqtt.UseDisconnectedHandler(e =>
            {
                _logger.LogWarning(e.Exception,
                    $"Mqtt disconnected '{_config.Host}:{_config.Port}'");
            });
        }

        public Task ConnectAsync()
        {
            return _mqtt.StartAsync(_mqttOptions);
        }

        public Task SubscribeAsync(string topic)
        {
            return _mqtt.SubscribeAsync(topic);
        }

        
        public Task StopAsync()
        {
            return _mqtt.StopAsync();
        }

        protected virtual void MqttMessage(Message e)
        {
            OnMqttMessage?.Invoke(this, e);
        }
    }
}
