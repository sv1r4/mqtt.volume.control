﻿namespace mqtt.volume.control.constants
{
    public static class Topic
    {
        public static readonly string Event = "e";
        public static readonly string Command = "c";
        public static readonly string VolumeValue = "value";
    }
}
