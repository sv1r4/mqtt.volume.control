﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mqtt.volume.control.config;
using mqtt.volume.control.exceptions;

namespace mqtt.volume.control.alsa
{
    public class AlsaCliWrapper:IVolumeControl
    {
        private readonly ILogger _logger;
        private static readonly int CommandTimeoutMs = 1000;
        private readonly AlsaConfig _alsaConfig;

        public AlsaCliWrapper(ILogger<AlsaCliWrapper> logger, IOptions<AlsaConfig> alsaConfig)
        {
            _logger = logger;
            _alsaConfig = alsaConfig.Value;
        }


        public Task SetVolumeAsync(int volPercent)
        {
            if (volPercent < 0 || volPercent > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(volPercent),"percent should be in range [0,100]");
            }
            SendAmixerCommand($"sset {_alsaConfig.Device} {volPercent}%");
            return Task.CompletedTask;
        }

        private void SendAmixerCommand(string args)
        {
            using (var p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.RedirectStandardError = true;
                p.OutputDataReceived += (sender, data) =>
                {
                    _logger.LogDebug(data.Data);
                };
                p.ErrorDataReceived += (sender, data) =>
                {
                    _logger.LogError(data.Data);
                    throw new AmixerException(data.Data);
                };
                p.StartInfo.FileName = "amixer";
                p.StartInfo.Arguments = args;
                p.Start();
                p.WaitForExit(CommandTimeoutMs);
            }
        }
    }
}
