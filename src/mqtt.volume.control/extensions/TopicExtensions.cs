﻿using mqtt.volume.control.constants;

namespace mqtt.volume.control.extensions
{
    public static class TopicExtensions
    {
        public static string CommandSetVolume(this string baseTopic)
        {
            return $"{NormalizeTopic(baseTopic)}/{Topic.Command}/{Topic.VolumeValue}";
        }

        private static string NormalizeTopic(string topic)
        {
            return topic.TrimEnd('/');
        }

        public static string CommandWildcard(this string baseTopic)
        {
            return $"{NormalizeTopic(baseTopic)}/{Topic.Command}/#";
        }
    }
}
