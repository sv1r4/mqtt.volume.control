﻿using System.Threading.Tasks;

namespace mqtt.volume.control
{
    public interface IVolumeControl
    {
        Task SetVolumeAsync(int volPercent);
    }
}
