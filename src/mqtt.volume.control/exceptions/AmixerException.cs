﻿using System;

namespace mqtt.volume.control.exceptions
{
    public class AmixerException:Exception
    {
        public AmixerException(string rawOutput):base(rawOutput)
        {
            RawOutput = rawOutput;
        }

        public string RawOutput { get; }
    }
}
